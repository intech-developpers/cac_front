import { ToastProgrammatic as Toast } from "buefy";
import Vue from "vue";
// init state
const state = () => ({
 
  isGeneralPopUpModalOpened: false,
  hasPopUpBeenShowed: false
});

//getters
const getters = {
  getGeneralPopUpModalOpened: (state) => {
    return state.isGeneralPopUpModalOpened;
  },

  hasPopUpBeenShowed: (state) => {
    return state.hasPopUpBeenShowed;
  },
 
};

//mutations
const mutations = {
  OPEN_GENERAL_POPUP_MODAL(state) {
    state.isGeneralPopUpModalOpened = true;
    state.hasPopUpBeenShowed = true
  },
  CLOSE_GENERAL_POPUP_MODAL(state) {
    state.isGeneralPopUpModalOpened = false;
  },
 
};

//actions

const actions = {
  setGeneralPopUpModalState({ state, commit }) {
    commit('OPEN_GENERAL_POPUP_MODAL');
  },
  closeGeneralPopUpModalState({ state, commit }) {
    commit('CLOSE_GENERAL_POPUP_MODAL');
  },
 
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
